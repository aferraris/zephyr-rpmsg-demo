/*
 * Copyright (c) 2020 Collabora Ltd
 *
 * SPDX-License-Identifier: LGPL-2.1
 */

#include "demo.h"

#include <device.h>
#include <drivers/sensor.h>
#include <stdio.h>

#define PI                  3.141592
#define VALUE_THRESHOLD     0.05
#define CALIBRATION_SAMPLES	16

struct imu_data {
    const struct device *dev;
    double calib[MYAPP_AXIS_COUNT];
    int64_t last_measure;
};

static double rad_to_deg(double rad)
{
    if (rad > 0)
        return (rad * 180. + PI / 2.) / PI;
    else
        return (rad * 180. - PI / 2.) / PI;
}

static void calibrate_imu(struct imu_data *imu)
{
    struct sensor_value values[MYAPP_AXIS_COUNT];
    double accu[MYAPP_AXIS_COUNT];
    int i, axis, ret;

    for (axis = 0; axis < MYAPP_AXIS_COUNT; axis++)
        accu[axis] = 0.0;

    /* Accumulate sample values for calibration */
    for (i = 0; i < CALIBRATION_SAMPLES; i++) {
        ret = sensor_sample_fetch(imu->dev);
        if (ret)
            continue;

        ret = sensor_channel_get(imu->dev, SENSOR_CHAN_GYRO_XYZ, values);
        if (ret)
            continue;

        for (axis = 0; axis < MYAPP_AXIS_COUNT; axis++)
            accu[axis] += sensor_value_to_double(&values[axis]);

        k_sleep(K_MSEC(100));
    }

    /* Calculate mean value for each axis and use it as calibration data */
    for (axis = 0; axis < MYAPP_AXIS_COUNT; axis++)
        imu->calib[axis] = accu[axis] / i;
}

static void calculate_orientation(struct imu_data *imu, struct app_data *data)
{
    struct sensor_value sensor_values[MYAPP_AXIS_COUNT];
    double values[MYAPP_AXIS_COUNT];
    double elapsed, val;
    int i;

    if (sensor_sample_fetch(imu->dev) != 0) {
        printk("Unable to fetch sensor data!\n");
        return;
    }

    if (sensor_channel_get(imu->dev, SENSOR_CHAN_GYRO_XYZ, sensor_values) != 0) {
        printk("Unable to get gyroscope values!\n");
        return;
    }

    /* Measure elapsed time since last measurement and convert to seconds */
    elapsed = (double)k_uptime_delta(&imu->last_measure);
    elapsed /= 1000.;

    if (elapsed < 0.001)
        /* Do nothing if elapsed time is less than 1ms */
        return;

    /* Update last measurement time */
    imu->last_measure = k_uptime_get();

    /* Convert angular velocity (rad/s) in degrees based on elapsed time */
    for (i = 0; i < MYAPP_AXIS_COUNT; i++) {
        val = sensor_value_to_double(&sensor_values[i]) - imu->calib[i];

        /* Filter out low values as they're likely just noise */
        if (val > VALUE_THRESHOLD || val < -VALUE_THRESHOLD )
            values[i] = rad_to_deg(val * elapsed);
        else
            values[i] = 0.0;
    }

    /* Update current orientation */
    k_mutex_lock(&data->mutex, K_FOREVER);
    for (int i = 0; i < MYAPP_AXIS_COUNT; i++) {
        data->angle[i] += values[i];
    }
    k_mutex_unlock(&data->mutex);
}

void imu_task(void *appdata, void *arg2, void *arg3)
{
    ARG_UNUSED(arg2);
    ARG_UNUSED(arg3);
    const char *const label = DT_LABEL(DT_INST(0, invensense_mpu6050));
    struct imu_data imu;

    imu.dev = device_get_binding(label);
    if (!imu.dev) {
        printk("Failed to find sensor %s\n", label);
        return;
    }

    calibrate_imu(&imu);
    /* Store current uptime as calculate_orientation() relies on it */
    imu.last_measure = k_uptime_get();
    for (;;) {
        calculate_orientation(&imu, (struct app_data *)appdata);
        k_sleep(K_MSEC(10));
    }
}
