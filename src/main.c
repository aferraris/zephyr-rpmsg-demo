/*
 * Copyright (c) 2020 Collabora Ltd
 *
 * SPDX-License-Identifier: LGPL-2.1
 */

#include "demo.h"

#define APP_TASK_STACK_SIZE (512)

K_THREAD_STACK_DEFINE(imu_thread_stack, APP_TASK_STACK_SIZE);
static struct k_thread imu_thread_data;

K_THREAD_STACK_DEFINE(ipc_thread_stack, APP_TASK_STACK_SIZE);
static struct k_thread ipc_thread_data;

void main(void)
{
    struct app_data *mydata;

    mydata = k_malloc(sizeof(struct app_data));
    if (!mydata) {
        printk("Unable to allocate memory!\n");
        return;
    }

    k_mutex_init(&mydata->mutex);
    for (int i = 0; i < MYAPP_AXIS_COUNT; i++)
        mydata->angle[i] = 0.0;

    printk("Starting IMU thread!\n");
    k_thread_create(&imu_thread_data, imu_thread_stack, APP_TASK_STACK_SIZE,
                    (k_thread_entry_t)imu_task, mydata, NULL, NULL,
                    K_PRIO_COOP(7), 0, K_NO_WAIT);

    printk("Starting IPC thread!\n");
    k_thread_create(&ipc_thread_data, ipc_thread_stack, APP_TASK_STACK_SIZE,
                    (k_thread_entry_t)ipc_task, mydata, NULL, NULL,
                    K_PRIO_COOP(7), 0, K_NO_WAIT);
}
