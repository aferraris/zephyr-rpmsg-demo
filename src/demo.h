#ifndef __MYAPP_H
#define __MYAPP_H

#include <zephyr.h>

enum {
    MYAPP_AXIS_X = 0,
    MYAPP_AXIS_Y,
    MYAPP_AXIS_Z,

    MYAPP_AXIS_COUNT
};

struct app_data {
    struct k_mutex mutex;
    double         angle[MYAPP_AXIS_COUNT];
};

void imu_task(void *appdata, void *arg2, void *arg3);
void ipc_task(void *appdata, void *arg2, void *arg3);

#endif
