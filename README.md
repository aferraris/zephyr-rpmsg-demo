# Zephyr RPMSG Demo #


## Overview ##

This application demonstrates how to take advantage of inter-processor
communication on the STM32MP157C-DK2 board. It processes data from an MPU6050
attached to I2C5 and sends the current sensor orientation to the Linux system
(running on the Cortex-A7 cores) on-demand.

## Build instructions ##

Make sure you have installed all Zephyr's
[dependencies](https://docs.zephyrproject.org/latest/getting_started/index.html)
first, as well as an ARMv7 cross-compilation toolchain. On Debian-based systems,
the toolchain can be installed by executing the following command:

```
sudo apt install gcc-arm-none-eabi
```

Then download the latest Zephyr sources (`ZEPHYR_DIR` being a directory of your
choice):

```
west init ZEPHYR_DIR
cd ZEPHYR_DIR
west update
```

Next, clone this project:

```
git clone https://gitlab.collabora.com/aferraris/zephyr-rpmsg-demo.git
```

Then setup the Zephyr environment:

```
. zephyr/zephyr-env.sh
export ZEPHYR_TOOLCHAIN_VARIANT=cross-compile
export CROSS_COMPILE=/usr/bin/arm-none-eabi-
```

Finally, you can build the application using the following command:

```
west build -p auto -b stm32mp157c_dk2 zephyr-rpmsg-demo
```

This will produce the file `ZEPHYR_DIR/build/zephyr/zephyr-rpmsg-demo.elf`.

## Running the application ##

Copy `zephyr-rpmsg-demo.elf` to your target device's `/lib/firmware` directory,
then execute the following commands:

```
echo zephyr-rpmsg-demo.elf > /sys/class/remoteproc/remoteproc0/firmware
echo start > /sys/class/remoteproc/remoteproc0/state
```

If you have installed the
[RPMSG client driver](https://gitlab.collabora.com/aferraris/zephyr-rpmsg-client),
you will be able to query the current orientation by reading
`/sys/bus/rpmsg/drivers/rpmsg_zephyr_client/values`.
